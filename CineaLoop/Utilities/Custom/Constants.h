//
//  Constants.h
//
//  Created by Akshay Gohel on 01/03/15.
//  Copyright (c) 2015 Akshay Gohel. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonSettings.h"

#define APP_VERSION @"1.0"

#define LOG_IT

#ifdef LOG_IT
#	define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#	define DLog(...)
#endif

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

// iPhone4/4s check
#define IS_SMALLER_SCREEN (APP_DELEGATE.window.frame.size.height == 480.0f)

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define COMMON_SETTINGS ((CommonSettings*)[CommonSettings sharedInstance])

#define TIME_OUT_INTERVAL 30.0f

#define API_BASE_URL @""

