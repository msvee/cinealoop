//
//  CommonSettings.m
//
//  Created by Akshay Gohel on 01/03/15.
//  Copyright (c) 2015 Akshay Gohel. All rights reserved.
//

#import "CommonSettings.h"

@interface CommonSettings ()

@property(nonatomic, strong) UIView *progressView;
@property(nonatomic, strong) UIAlertView *alertView;

@end

@implementation CommonSettings

static CommonSettings *sharedInstance;

+(CommonSettings*) sharedInstance {
    if (!sharedInstance) {
        sharedInstance = [[CommonSettings alloc] init];
        [sharedInstance resetContents];
    }
    return sharedInstance;
}

-(void) resetContents {
    [COMMON_SETTINGS hideActivityIndicator];
}

-(void) showActivityIndicator {
    if (self.progressView) {
        [self hideActivityIndicator];
    }
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    id windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *w in windows) {
        if (w.isKeyWindow) {
            window = w;
            break;
        }
    }
    self.progressView = [[UIView alloc] initWithFrame:window.frame];
    self.progressView.backgroundColor = [UIColor clearColor];
    self.progressView.tag = 987123;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    activityIndicator.color = [UIColor lightGrayColor];
    activityIndicator.center = APP_DELEGATE.window.center;
    activityIndicator.tag = 123789;
    [self.progressView addSubview:activityIndicator];
    [window addSubview:self.progressView];
    [activityIndicator startAnimating];
    self.progressView.userInteractionEnabled = YES;
}

-(void) hideActivityIndicator {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    id windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *w in windows) {
        if (w.isKeyWindow) {
            window = w;
            break;
        }
    }
    for (UIView *v in window.subviews) {
        if (v.tag == 987132) {
            UIView *activityIndicator = [v viewWithTag:123789];
            if (activityIndicator && [activityIndicator isKindOfClass:[UIActivityIndicatorView class]]) {
                [((UIActivityIndicatorView*)activityIndicator) stopAnimating];
                [((UIActivityIndicatorView*)activityIndicator) removeFromSuperview];
            }
            
        }
    }
    self.progressView.userInteractionEnabled = NO;
    [self.progressView removeFromSuperview];
    self.progressView = nil;
}

-(void) showAppologiesAlertWithTitle:(NSString*) title andMessage:(NSString*) message andDelegate:(id)delegate {
    if (!self.alertView.isVisible) {
        if (title == nil) {
            title = @"Apologies!";
        }
        if (message == nil) {
            message = @"There is some error. Please try again later.";
        }
        self.alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
        [self.alertView show];
    }
}

//-(UIColor *)colorFromHexString:(NSString *)hexString {
//    hexString = @"ffffff";//hexString.lowercaseString;
//    unsigned rgbValue = 0;
//    NSScanner *scanner = [NSScanner scannerWithString:hexString];
//    [scanner setScanLocation:1]; // bypass '#' character
//    [scanner scanHexInt:&rgbValue];
//    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
//}

-(UIColor *) colorFromHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
//            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

-(CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
