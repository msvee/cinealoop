//
//  CommonSettings.h
//
//  Created by Akshay Gohel on 01/03/15.
//  Copyright (c) 2015 Akshay Gohel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonSettings : NSObject

+(CommonSettings*) sharedInstance;
-(void) resetContents;

-(void) showActivityIndicator;
-(void) hideActivityIndicator;

-(void) showAppologiesAlertWithTitle:(NSString*) title andMessage:(NSString*) message andDelegate:(id)delegate;

-(UIColor *)colorFromHexString:(NSString *)hexString;

@end
