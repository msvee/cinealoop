//
//  CollectionTableViewCell.h
//  HomeVedaApp
//
//  Created by Akshay Gohel on 23/06/15.
//  Copyright (c) 2015 Akshay Gohel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property (weak, nonatomic) IBOutlet UILabel *articleCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
