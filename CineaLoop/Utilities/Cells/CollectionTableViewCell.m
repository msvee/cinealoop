//
//  CollectionTableViewCell.m
//  HomeVedaApp
//
//  Created by Akshay Gohel on 23/06/15.
//  Copyright (c) 2015 Akshay Gohel. All rights reserved.
//

#import "CollectionTableViewCell.h"

@implementation CollectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
